# Simple usage

## Build progect

Run in console:

```
$ cargo build
```

Then yo can check for executable in `./target/debug`.
It is `./target/debug/guessing_game`.


## Run program

To run program you can use built executable (see previous section)
or you can run in console:

1. Case sensitive search:
```
$ cargo run <search-string> <filename>
```
2. Case insensitive search:
    * for Linux:
    ```
    $ CASE_INSENSITIVE=true cargo run <search-string> <filename>
    ```

    * for Windows:
    ```
    PS> $Env:CASE_INSENSITIVE=1; cargo run to poem.txt
    ```
    then unset `CASE_INSENSITIVE` environment variable:
    ```
    PS> Remove-Item Env:CASE_INSENSITIVE
    ```


## Run tests

Run in console:

```
$ cargo test
```
