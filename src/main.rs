use std::process;
use minigrep_243307::*;

fn main() {
    let config = parse_command_line_args().unwrap_or_else(|error_message| {
        eprintln!("Problem parsing arguments: {}", error_message);
        process::exit(1);
    });

    if let Err(message) = run(config) {
        eprintln!("Failed to read target file: {}", message);
        process::exit(1);
    }
}
