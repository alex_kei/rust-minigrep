use std::{env, fs, error::Error};

pub fn parse_command_line_args() -> Result<SearchConfig, &'static str> {
    
    SearchConfig::new(env::args())
}

pub struct SearchConfig {
    pub query: String,
    pub target_file: String,
    pub case_sensitive: bool,
}

impl SearchConfig {
    fn new(mut command_line_args: env::Args) -> Result<SearchConfig, &'static str> {
        command_line_args.next();

        let query = match command_line_args.next() {
            Some(arg) => arg,
            None => {
                return Err("Search query and target file are not provided.");
            }
        };

        let target_file = match command_line_args.next() {
            Some(arg) => arg,
            None => {
                return Err("Target file is not provided.");
            }
        };

        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();

        Ok(SearchConfig { query, target_file, case_sensitive })
    }
}

pub fn run(config: SearchConfig) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.target_file)?;

    let matched_lines = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in matched_lines {
        println!("{}", line);
    }

    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let lowered_case_query = query.to_lowercase();

    contents
        .lines()
        .filter(|line| line.to_lowercase().contains(&lowered_case_query))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
